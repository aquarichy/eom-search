# eom-search

eom-search allows you to search for sections in an Emacs Org Mode file that
match one or more regular expressions using AND or OR boolean logic, in either
the section's header or body.  Output includes the matching section's header and
the specific lines that match.

## License and Copyright

- Author: Richard Schwarting © 2023
- License: GPLv3

## Example

Here, doing a boolean OR search (-o) on the regular expressions "Second Cup" and
"Wallace", we find three matching sections on different dates. Section "**
<2022-02-03>" would also have been returned in a boolean AND search, since both
regular expressions match text in its body.

<pre>
$ ./eom_search.py work.org -o  "Second Cup" "Wallace"
     3412: ** <2022-02-03>
     |  3431:   - Wallace brought vegan cupcakes in to work.  Bless him.
     |  3444:     - I spilled my tea at Second Cup, I've been banned
     6070: ** <2021-11-09>
     |  6124:   - Wallace asked me to re-cragnify the gurbulator reports
     6503: ** <2021-02-22>
     |  6506:   - shamefully bought a disposable cup from Second Cup :(
</pre>

## Use case

I use Org Mode files to organize most things in my life. E.g. I have a 110,000+
line personal journal that provides daily log as well as project sections.  I
had a 120,000+ file organizing projects, tasks and a daily log from a 7 year
job.

Often, I need to quickly identify which day or project some activity occurred.
Ultimately, grep works, but it is a bit inefficient, especially when trying to identify
sections where two subjects co-occur (but on different lines).

## Todo

- Add a context flag, allowing the user to specify a number of lines before and after to show of matching lines.
- Add a depth flag, allowing the user to specify how many parent section headers to show
- Add option to limit co-occurrence searches (AND) to just direct ancestors, ignoring siblings.

### Direct ancestors feature idea

Within a section's body, text can be structured as nested lists.  For direct ancestor matching on multiple
subjects (regular expressions), only show if one is the ancestor in such a list or a header.  Here is an example.

<pre>
* work log
** 2020-02-03
- hired Wallace Wells
- Second Cup makes a great hot chocolate
** 2020-02-02
- interviewed Wallace Wells
  - met up at Second Cup
</pre>

If I wanted to only find records that connected Wallace and Second Cup, the flag would allow you to do:

<code>eom-search work.org -d "Wallace" "Second Cup"</code>

And it would only match the section from 2020-02-02.
