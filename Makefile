INST_DIR=${HOME}/.local/bin

check: pylint mypy

pylint: .pylint_passed

mypy: .mypy_passed

.pylint_passed: .pylintrc eom_search.py
	pylint eom_search.py
	touch .pylint_passed

.mypy_passed: eom_search.py
	mypy eom_search.py
	touch .mypy_passed

$(INST_DIR)/eom_search.py: eom_search.py
	install eom_search.py "$(INST_DIR)/"

install: $(INST_DIR)/eom_search.py

uninstall:
	rm -f "$(INST_DIR)/eom_search.py";

clean:
	find . -type f -name "*~" -delete -or -name "#*#" -delete
	rm -f .pylint_passed .mypy_passed
