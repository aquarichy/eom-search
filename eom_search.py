#!/bin/env python3

"""eom_search.py

Search emacs org-mode files.  Accepts one or more terms.  Locates sections that
match all the terms (AND, default) or any of the terms (OR, -o) inside the
header or body.  Prints section header and matching lines with line numbers.

Author: Richard Schwarting <aquarichy@gmail.com> © 2023
License: GPLv3+
"""

import re;
import argparse;
import configparser;
import sys;
import os;
import textwrap;

from dataclasses import dataclass;
from typing import Optional;

@dataclass
class Line:
    """Represents a line from the file with line number"""
    num: int;
    content: str;

@dataclass
class Section:
    """Represents and emacs org-mode section, containing the header, a list of body lines, its depth
       and a list of any children and a link to its parent.  The root Section has no header or parent.
       Sections are headed with a line led by a number of '*' that represent its depth."""
    header: Line;
    depth: int;
    body: list[Line]; # list of lines
    children: list['Section'];
    parent: Optional['Section'];

class DepthException(Exception):
    """Exception for depth errors (e.g. sections are improperly nested)"""

def parse_file (fin) -> Section:
    """Given a file handle, fin, this parses he entire file and returns the root
       Section, which which all sub-Sections can be reached.

       Arguments:
         fin: a file handle

       Returns: the root Section"""
    header_re : re.Pattern[str] = re.compile (r"^(\*+) (.*)$");
    root : Section = Section (Line (-1, "<root>"), 0, [], [], None );
    cur : Section = root;
    line_num : int = 0;

    for line in fin:
        line = line.rstrip (); # remove trailing \n

        matches : re.Match | None = header_re.match (line);
        if matches:
            header_rec = Line (line_num, matches[2]);
            new : Section = Section (header_rec, len(matches[1]), [], [], None);

            if new.depth == cur.depth + 1:
                pass;
            elif new.depth <= cur.depth:
                for _ in range (0, cur.depth - new.depth + 1):
                    if cur.parent is None:
                        raise DepthException (f"ERROR: depth error 2, cur's depth: {cur.depth}, new depth {new.depth}, line {line_num}: '{line}'");
                    cur = cur.parent;
            else:
                raise DepthException (f"ERROR: depth error 1, cur's depth: {cur.depth}, new depth {new.depth}, line {line_num}: '{line}'");

            cur.children.append (new);
            new.parent = cur;
            cur = new;
        else:
            line_rec = Line (line_num, line);
            cur.body.append (line_rec);

        line_num += 1;

    return root;

def print_matching_lines (cur : Section, all_matching_lines : dict[int,str], args : argparse.Namespace) -> None:
    """Given a section with matched lines, print its header(s) and the matched lines

       Arguments
         cur: the matching Section.
         all_matching_lines: a mapping of line number to line string content.
         args: the argparse.Namespace capturing options such as `start` or `ancestors`
    """
    indent : str = (" " * cur.depth);
    header_lines : list[str] = [];

    if args.ancestors > 1 or args.start:
        print (f"{indent}--");

    depth : int;
    if args.start:
        depth = max(cur.depth - args.start + 1, 1);
    else:
        depth = args.ancestors;

    header : Section = cur;
    for _ in range (0, depth):
        header_lines.append (f"{indent}{header.header.num if header.header.num > -1 else '':5}: {'*' * header.depth} {header.header.content}");
        if header.parent and header.parent.header.num > -1:
            header = header.parent;
        else:
            break;

    for line in reversed (header_lines):
        print (line);


    for num, content in all_matching_lines.items ():
        print (f"{indent}{num:5}| {content}");

def find_sections_where_body_has_poly (cur : Section, subject_res : list[re.Pattern[str]], args : argparse.Namespace) -> None:
    """Takes a Section and a list of search patterns, and displays any matches
       found.  If match_all is True ('AND') then all provided search patterns
       must find at least one match within the section's header or body.  If
       match_all is False ('OR') than only at least one pattern must find a match.

       This recurses through all of Section cur's children.

       Arguments:
         cur: the current Section.
         subject_res: a list of Patterns to match against the current section's header and body.
         args: the argparse.Namespace capturing options such as match_any, ancestors, and start.

       Returns: None
    """
    all_matched : bool = True;
    any_matched : bool = False;
    all_matching_lines : dict[int,str] = {};

    for subject_re in subject_res:
        header_matched : bool = False;
        if subject_re.match (cur.header.content):
            header_matched = True;

        matching_lines : dict[int,str] = section_body_get_matches (cur, subject_re);

        if len(matching_lines) == 0 and not header_matched:
            all_matched = False;
        else:
            any_matched = True;

        all_matching_lines.update (matching_lines);

    if (not args.match_any and all_matched) or (args.match_any and any_matched):
        print_matching_lines (cur, all_matching_lines, args);

    for child in cur.children:
        find_sections_where_body_has_poly (child, subject_res, args);

def section_body_get_matches (cur : Section, subject_re : re.Pattern[str]) -> dict[int,str]:
    """Traverses a section's body to collect matches.

       Arguments:
         cur: the current Section.
         subject_re: a Pattern to match against Section cur's body.

       Returns: a dictionary mapping line numbers to line content.
    """
    matching_lines : dict[int,str] = {};

    for line in cur.body:
        if subject_re.match (line.content):
            matching_lines[line.num] = line.content;

    return matching_lines;

def main () -> None:
    """Parse arguments and then search the provided emacs org-mode file for the provided terms"""
    root : Section;

    # Parse config
    config : configparser.ConfigParser = configparser.ConfigParser ();
    config.read_string (textwrap.dedent ('''\
                                         [DEFAULT]
                                         match_any = no
                                         num_ancestors = 1
                                         start = 1
                                         ignore_case = no

                                         [Default]
                                         '''));
    config.read (os.path.expanduser ("~/.config/eom-search/config.ini"));

    # Parse arguments
    parser : argparse.ArgumentParser = argparse.ArgumentParser (prog = "eom_search",
                                                                formatter_class = argparse.RawTextHelpFormatter,
                                                                description = "Search for text in an Emacs Org-Mode file",
                                                                epilog=textwrap.dedent (f'''\
                                                                CONFIG FILE

                                                                Certain settings can be stored in ~/.config/eom-search/config.ini so that they
                                                                do not need to be specified via the command-line each time.  Example file:

                                                                  [Default]
                                                                  # org_filepath = /path/to/file.org  # -f, --filepath
                                                                  match_any = {config.getboolean ("DEFAULT", "match_any")}                   # -o, --or
                                                                  ignore_case = {config.getboolean ("DEFAULT", "ignore_case")}                 # -i, --ignore-case
                                                                  num_ancestors = {config.getint ("DEFAULT", "num_ancestors")}                   # -a, --ancestors
                                                                  start = {config.getint ("DEFAULT", "start")}                           # -s, --start
                                                                '''));
    parser.add_argument ("-f", "--filepath", default = config.get ('Default', 'org_filepath', fallback=None),
                         help = "An emacs org-mode file path to search.  Must be specified here, or in CONFIG FILE as 'org_filename'");
    parser.add_argument ("term", nargs = "+",
                         help = "One or more terms to search for (default: boolean AND)");
    parser.add_argument ("-o", "--or", dest = "match_any", action = "store_true", default = config.getboolean ('Default', 'match_any'),
                         help = "Boolean OR matching; if multiple terms are provided, only one needs to match.");
    parser.add_argument ("-i", "--ignore-case", action = "store_true", default = config.getboolean ('Default', 'ignore_case'),
                         help = "Ignore case.  E.g. 'cat' will match 'Cat' and 'CAT'");

    depth_group : argparse._MutuallyExclusiveGroup = parser.add_mutually_exclusive_group ();
    depth_group.add_argument ("-a", "--ancestors", default = config.getint ('Default', 'num_ancestors'), type=int,
                              help = "How many ancestors deep to print headers for.  0: show no headers, 1: show immediate header, 2: show immediate header and its parent, etc.");
    depth_group.add_argument ("-s", "--start", default = config.getint ('Default', 'start'), type=int,
                              help = "Print all headers starting from section START.  E.g. If set to 2, and a match is found at depth 5, it will print headers at depths 2, 3, 4, and 5");

    args : argparse.Namespace = parser.parse_args ();

    if args.ancestors < 0:
        print ("ERROR: depth must be non-negative.", file=sys.stderr);
        sys.exit (1);

    if args.filepath is None:
        print ("ERROR: org-mode file path not provided.  Must be specified on the command-line or in a config file.", file=sys.stderr);
        sys.exit (2);

    with open (args.filepath, "r", encoding = "utf-8") as fin:
        root = parse_file (fin);

    subject_res : list[re.Pattern[str]] = [ re.compile (f".*{x}.*", re.IGNORECASE if args.ignore_case else re.NOFLAG) for x in args.term ];

    find_sections_where_body_has_poly (root, subject_res, args);

main ();
